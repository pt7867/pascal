/*
written by pt
aug 03
*/
var pascalApp = angular.module('pascalApp', ['ngRoute','ngSanitize']);

    // configure our routes
    pascalApp.config(function($routeProvider) {
        $routeProvider

            // route for the home page
            .when('/', {
                templateUrl : 'pages/pascalView.html',
                controller  : 'mainController'
            })
			.when('/:n', {
                templateUrl : 'pages/pascalView.html',
                controller  : 'mainController'
            })

    });

    
    pascalApp.controller('mainController', function($scope, $routeParams, $location) {
             
		if( $routeParams.n){
			$scope.pnumber = $routeParams.n;
			console.log($scope.pnumber);
			 var n = parseInt($scope.pnumber);
		     var str ='';
            for (var line = 1; line <= n; line++)
            {
              var color = "";
              if(line%4 == 1){
                color = "red";
              }
              if(line%2 == 0){
                color = "blue";
              }
              if(line%2 == 1 && line%4 != 1){
                color = "orange";
              }
              if(line%4 == 0){
                color = "green";
              }

              var C = 1;
			  str = str+'<div class="ps-' + color +'"  >';
              for (var i = 1; i <= line; i++)
              {
                 str = str+C;
                C = C * (line - i) / i;
              }
             
			   str = str+'</div><br>';
            }
			
			$scope.pascal = str;
		}
        $scope.printpascal = function(){
			$location.path('/'+$scope.pnumber);
       
        };
  
    });
